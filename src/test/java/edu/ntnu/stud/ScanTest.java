package edu.ntnu.stud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ScanTest {
  Scan scan = new Scan();

  @Nested
  @DisplayName("Positive tests for the Scan class.")
  class PositiveScanTest {
  @Test
  @DisplayName("validTrainNumber does not throw 'Ill.Arg.Exc.' on valid train number.")
  void validTrainNumberPositive() {
    boolean validTrainNumber = scan.validateStringAsInteger("12344");
    if (!validTrainNumber) {
      fail("The method 'validTrainNumber' failed since the train number was invalid.");
    }
    assertTrue(validTrainNumber);
  }

  @Test
  @DisplayName("validDestination does not throw 'Ill.Arg.Exc.' on valid destination.")
  void validDestinationPositive() {
    boolean validDestiation = scan.validateLettersOnly("Oslo");
    if (!validDestiation) {
      fail("The method 'validDestination' failed since the destination was invalid.");
    }
    assertTrue(validDestiation);
  }

  @Test
  @DisplayName("validLine does not throw 'Ill.Arg.Exc.' on valid line.")
  void validLinePositive() {
    boolean validLine = scan.validateLettersAndDigitsOnly("VY10");
    if (!validLine) {
      fail("The method 'validLinePositive' failed since the line was invalid.");
    }
    assertTrue(validLine);
  }

  @Test
  @DisplayName("ValidateAdjustedDepartureTime does not throw when adjusted departure is at current day.")
  void validateAdjustedDepartureTimePositive() {
    LocalTime departureTime = LocalTime.of(20, 0);
    LocalTime delay = LocalTime.of(3, 0);
    boolean validAdjustedDepartureTime = scan.validateAdjustedDepartureTime(departureTime, delay);
    if (validAdjustedDepartureTime) {
      fail("The method 'validateAdjustedDepartureTimePositive' failed since valid time was invalid.");
    }
    assertFalse(validAdjustedDepartureTime);
  }

    @Test
    @DisplayName("ValidateStringLength does not throw when string is 5 characters or fewer.")
    void validateStringLengthPositive() {
      boolean validStringLength = scan.validateStringLength("12345");
      if (!validStringLength) {
        fail("The method 'validateStringLengthPositive' failed since the string length was invalid.");
      }
      assertTrue(validStringLength);
    }


}

  @Nested
  @DisplayName("Negative tests for the Scan class.")
  class NegativeScanTests {
    @Test
    @DisplayName("validTrainNumber returns false on invalid train number.")
    void validTrainNumberNegative() {
      boolean validTrainNumber = scan.validateStringAsInteger("100L");
      if (validTrainNumber) {
        fail("The method 'validTrainNumberNegative' failed since the train number was valid.");
      }
      assertFalse(validTrainNumber);
    }

    @Test
    @DisplayName("validDestination returns false on invalid destination.")
    void validDestinationNegative() {
      boolean validDestiation = scan.validateLettersOnly("Oslo1");
      if (validDestiation) {
        fail("The method 'validDestinationNegative' failed since the destination was valid.");
      }
      assertFalse(validDestiation);
    }

    @Test
    @DisplayName("validLine returns false on invalid line.")
    void validLineNegative() {
      boolean validLine = scan.validateLettersAndDigitsOnly(("VY-"));
      if (validLine) {
        fail("The method 'validLineNegative' failed since the line was valid.");
      }
      assertFalse(validLine);
    }

    @Test
    @DisplayName("ValidateStringLength returns false on invalid string length.")
    void validateStringLengthNegative() {
      boolean validString = scan.validateStringLength(("123456"));
      if (validString) {
        fail("The method 'validateStringLengthNegative' failed since the string length was valid.");
      }
      assertFalse(validString);
    }

    @Test
    @DisplayName("ValidateAdjustedDepartureTime returns false on invalid line.")
    void validateAdjustedDepartureTimeNegative() {
      boolean validAdjusetedDeparturetime =
          scan.validateAdjustedDepartureTime(LocalTime.of(20, 0), LocalTime.of(4, 0));
      if (!validAdjusetedDeparturetime) {
        fail("The method 'validateAdjustedDepartureTimeNegative' failed since the adjusted departure time was valid.");
      }
      assertTrue(validAdjusetedDeparturetime);
    }
  }
}
