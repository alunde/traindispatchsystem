package edu.ntnu.stud;

import org.junit.jupiter.api.*;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

public class TrainDepartureTest {
  LocalTime departureTime;
  String line;
  String trainNumber;
  String destination;
  LocalTime delay;
  int track;

  @BeforeEach
  @DisplayName("Default values for parameters.")
  void setUp () {
    departureTime = LocalTime.of(20, 0);
    line = "L4";
    trainNumber = "150";
    destination = "Oslo";
    delay = LocalTime.of(0, 5);
    track = -1;
  }

  @Nested
  @DisplayName("Positive tests for the TrainDeparture class.")
  class PositiveTrainDepartureTest {
    @Test
    @DisplayName("TrainDeparture constructor does not throw 'Ill.Arg.Exc.' on valid input.")
    void trainDepartureConstructorPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertNotNull(departure);
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("GetDepartureTime returns correct value.")
    void getDepartureTimePositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals(LocalTime.of(20, 0), departure.getDepartureTime());
      } catch (IllegalArgumentException e) {
        fail("The test 'getDepartureTimePositive' failed with the exception message: ", e);
      }
    }

    @Test
    @DisplayName("GetLine returns correct value.")
    void getLinePositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals("L4", departure.getLine());
      } catch (IllegalArgumentException e) {
        fail("The test 'getLinePositive' failed with the exception message: ", e);
      }
    }

    @Test
    @DisplayName("GetTrainNumber returns correct value.")
    void getTrainNumberPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals("150", departure.getTrainNumber());
      } catch (IllegalArgumentException e) {
        fail("The test 'getTrainNumberPositive' failed with the exception message: ", e);
      }
    }

    @Test
    @DisplayName("GetDestination returns correct value.")
    void getDestinationPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals("Oslo", departure.getDestination());
      } catch (IllegalArgumentException e) {
        fail("The test 'getDestinationPositive' failed with the exception message: ", e);
      }
    }

    @Test
    @DisplayName("GetDelay returns correct value.")
    void getDelayPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals(LocalTime.of(0, 5), departure.getDelay());
      } catch (IllegalArgumentException e) {
        fail("The test 'getDelayPositive' failed with the exception message: ", e);
      }
    }

    @Test
    @DisplayName("GetTrack returns correct value.")
    void getTrackPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals(-1, departure.getTrack());
      } catch (IllegalArgumentException e) {
        fail("The test 'getTrackPositive' failed with the exception message: ", e);
      }
    }

    @Test
    @DisplayName("SetDelay does not throw 'Ill.Arg.Exc.' on valid delay.")
    void setDelayPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        departure.setDelay(LocalTime.of(0, 10));
        assertEquals(LocalTime.of(0, 10), departure.getDelay());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("SetTrack does not throw 'Ill.Arg.Exc.' on valid track.")
    void setTrackPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        departure.setTrack(7);
        assertEquals(7, departure.getTrack());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("ToString method prints the train departure as expected.")
    void toStringPositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);

      String expectedString = """
          | 20:00          | L4      | 150            | Oslo           | 00:05 -> Expected departure: 20:05          |       |""";

      assertEquals(expectedString, departure.toString());
      if (!(expectedString.equals(departure.toString()))) {
        fail("The toString method for the item did not print the expected string.");
      }
    }

    @Test
    @DisplayName("TrainDeparture getter for adjusted departure time is calculating the expected output." +
        "for departure time 20:00 and delay 5 min.")
    void TrainDepartureGetterPositive1() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        assertEquals(LocalTime.of(20, 5), departure.getAdjustedDepartureTime());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture getter for adjusted departure time is calculating the expected output" +
        "for departure time 20:00 and delay 2 hours and 30 min.")
    void TrainDepartureGetterPositive2() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, LocalTime.of(2, 30), track);
        assertEquals(LocalTime.of(22, 30), departure.getAdjustedDepartureTime());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception-message: " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests for the TrainDeparture class.")
  class NegativeTrainDepartureTests {

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on blank line.")
    void trainDepartureConstructorThrowsOnBlankLine() {
      try {
        new TrainDeparture(
            departureTime, "", trainNumber, destination, delay, track);
        fail("The test 'trainDepartureConstructorThrowsOnBlankLine' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Line' was a blank string. "
            + "Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on blank train number.")
    void trainDepartureConstructorThrowsOnBlankTrainNumber() {
      try {
        new TrainDeparture(
            departureTime, line, "", destination, delay, track);
        fail("The test 'trainDepartureConstructorThrowsOnBlankTrainNumber' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' was a blank string."
            + " Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on blank destination. ")
    void trainDepartureConstructorThrowsOnBlankDestination() {
      try {
        new TrainDeparture(LocalTime.of(
            20, 0), "L4", "150", "", LocalTime.of(0, 5), -1);
        fail("The test 'trainDepartureConstructorThrowsOnBlankDestination' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Destination' was a blank string. "
            + "Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on train number that includes letters and symbols.")
    void trainDepartureConstructorThrowsOnInvalidTrainNumber() {
      try {
        new TrainDeparture(
            departureTime, line, "123a", destination, delay, track);
        fail("The test 'trainDepartureConstructorThrowsOnInvalidTrainNumber' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' "
            + "can only contain positive integer numbers.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on destination that includes numbers and symbols.")
    void trainDepartureConstructorThrowsOnInvalidDestination() {
      try {
        new TrainDeparture(
            departureTime, line, trainNumber, "Oslo1", delay, track);
        fail("The test 'trainDepartureConstructorThrowsOnInvalidTrack' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Destination' can only contain letters. "
            + "Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on invalid track.")
    void trainDepartureConstructorThrowsOnInvalidTrack() {
      try {
        new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, 11);
        fail("The test 'trainDepartureConstructorThrowsOnInvalidTrack' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The value for the parameter 'Track' was below 1 or above 10. "
            + "Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on string with over 5 characters.")
    void trainDepartureConstructorThrowsOnInvalidStringLength() {
      try {
        new TrainDeparture(
            departureTime, line, "123456", destination, delay, track);
        fail("The test 'trainDepartureConstructorThrowsOnInvalidStringLength' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' can maximum have 5 characters.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture-constructor throws 'Ill.Arg.Exc.' on invalid string.")
    void trainDepartureConstructorThrowsOnInvalidString() {
      try {
        new TrainDeparture(
            departureTime, "-VY10", trainNumber, destination, delay, track);
        fail("The test 'trainDepartureConstructorThrowsOnInvalidString' failed since it did not throw "
            + "'IllegalArgumentException' as expected.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Line' can only contain letters and numbers.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainDeparture getter for adjusted departure time is calculating the expected output" +
        "for departure time 20:00 and delay 4 hours and 30 min.")
    void TrainDepartureGetterNegative() {
      try {
        new TrainDeparture(
            departureTime, line, trainNumber, destination, LocalTime.of(4, 30), track);
        fail("The method 'TrainDepartureGetterNegative' failed since 'Ill.Arg.Exc.' was not thrown when departure time plus"
            + " delay exceeds the current day.");
      } catch (IllegalArgumentException e) {
        assertEquals("Departure time plus delay must be at current day.", e.getMessage());
      }
    }

    @Test
    @DisplayName("VerifyTime throws when LocalTime is null")
    void verifyTimeNegative() {
      try {
        new TrainDeparture(
            departureTime, line, trainNumber, destination, null, track);
        fail("verifyTimeNegative failed since 'Ill.Arg.Exc' was not thrown.");
      } catch (IllegalArgumentException e) {
        assertEquals("Delay cannot be null", e.getMessage());
      }
    }
  }
}
