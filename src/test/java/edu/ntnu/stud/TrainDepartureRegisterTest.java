package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TrainDepartureRegisterTest {
  TrainDepartureRegister register;

  LocalTime departureTime;
  String line;
  String trainNumber;
  String destination;
  LocalTime delay;
  int track;

  @BeforeEach
  @DisplayName("")
  void setUp () {
    departureTime = LocalTime.of(20, 0);
    line = "L4";
    trainNumber = "150";
    destination = "Oslo";
    delay = LocalTime.of(0, 5);
    track = -1;
    register = new TrainDepartureRegister();
  }

  @Nested
  @DisplayName("Positive tests for the TrainDepartureRegister class.")
  class PositiveTrainDepartureRegisterTest {
    @Test
    @DisplayName("GetTrainDepartures does not throw 'Ill.Arg.Exc.' on train departures.")
    void getTrainDeparturesPositive() {
      TrainDeparture departure = new TrainDeparture(
          LocalTime.of(17, 0), line, trainNumber, destination, delay, track);
      TrainDeparture departure1 = new TrainDeparture(
          LocalTime.of(15, 0), line, "1", destination, delay, track);
      register.addTrainDeparture(departure);
      register.addTrainDeparture(departure1);
      List<TrainDeparture> departures = register.getTrainDeparturesSorted();
      assertEquals(departure, departures.get(1), "Departure should be in index 1");
      assertEquals(departure1, departures.get(0), "Departure1 should be in index 0");
    }

    @Test
    @DisplayName("RemoveIfDeparted does not throw 'Ill.Arg.Exc.' on train departures removed when departed.")
    void removeIfDepartedPositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      TrainDeparture departure1 = new TrainDeparture(
          LocalTime.of(14, 0), line, "1", destination, delay, track);
      register.addTrainDeparture(departure);
      register.addTrainDeparture(departure1);
      LocalTime time = LocalTime.of(16, 0);
      register.removeIfDeparted(time);
      List<TrainDeparture> trainDeparturesRemaining = register.getTrainDeparturesSorted();
      assertEquals(1, trainDeparturesRemaining.size(), "One train departure should be removed." );
      assertTrue(trainDeparturesRemaining.contains(departure), "'Departure' is not in the list.");
      assertFalse(trainDeparturesRemaining.contains(departure1), "'Departure1' is in the list.");

    }
    @Test
    @DisplayName("AddTrainDeparture does not throw 'Ill.Arg.Exc' on valid train departure added.")
    void addTrainDeparturePositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      List<TrainDeparture> trainDeparturesRemaining = register.getTrainDeparturesSorted();

      assertEquals(1, trainDeparturesRemaining.size() , "One registered train departure in the register.");

    }

    @Test
    @DisplayName("RemoveTrainDeparture does not throw 'Ill.Arg.Exc.' on train departures being removed.")
    void removeTrainDeparturePositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);

      register.removeTrainDeparture(trainNumber);
      List<TrainDeparture> trainDeparturesRemaining = register.getTrainDeparturesSorted();

      assertEquals(0, trainDeparturesRemaining.size() , "No registered train departures in the register.");
    }
    @Test
    @DisplayName("SearchForTrain does not throw 'Ill.Arg.Exc.' on valid train number.")
    void searchForTrainNumberPositive() {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);
        TrainDeparture trainDepartureFound = register.searchForTrainNumber("150");
        if (trainDepartureFound == null) {
          fail("The method 'searchForTrainNumber' failed since it did not find the traindeparture"
              + " when existed in the register.");
        }
        assertEquals(departureTime, departure.getDepartureTime());
        assertEquals(line, departure.getLine());
        assertEquals(trainNumber, departure.getTrainNumber());
        assertEquals(destination, departure.getDestination());
        assertEquals(delay, departure.getDelay());
        assertEquals(track, departure.getTrack());


    }

    @Test
    @DisplayName("SearchForDestination does not throw 'Ill.Arg.Exc.' on train departures with a specific destination.")
    void searchForDestinatnionPostivtive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      TrainDeparture departure1 = new TrainDeparture(
          departureTime, line, "1", destination, delay, track);
      register.addTrainDeparture(departure);
      register.addTrainDeparture(departure1);

      List<TrainDeparture> departures = new ArrayList<>();
      departures.add(departure);
      departures.add(departure1);
      List<TrainDeparture> getDepartures = register
          .searchForDestination(departure.getDestination());

      assertEquals(departures, getDepartures);
    }

    @Test
    @DisplayName("AdjustDelay does not throw 'Ill.Arg.Exc.' on valid input.")
    void adjustDelayPositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      register.adjustDelay(trainNumber, LocalTime.of(1, 30));
      assertEquals(LocalTime.of(1, 30), departure.getDelay());
    }

    @Test
    @DisplayName("AssignTrack does not throw 'Ill.Arg.Exc.' on valid input,")
    void assignTrackPositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      register.assignTrack(trainNumber, 3);
      assertEquals(3, departure.getTrack());
    }
    @Test
    @DisplayName("TrainNumberExists does not throw 'Ill.Arg.Exc.' on train number that exists.")
    void trainNumberExistsPositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      boolean noneExistingTrainNumber = register.trainNumberExists(trainNumber);
      if (!noneExistingTrainNumber) {
        fail("The method 'trainNumberExists' failed since it did not find the existing train number.");
      }
      assertTrue(noneExistingTrainNumber);
    }

    @Test
    @DisplayName("DestinationExists does not throw 'Ill.Arg.Exc.' on destination that exists.")
    void destinationExistsPositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      boolean noneExistingDestination = register.destinationExists(destination);
      if (!noneExistingDestination) {
        fail("The method 'destinationExistsPositive' failed since it did not find the existing destination.");
      }
      assertTrue(noneExistingDestination);
    }

    @Test
    @DisplayName("GetDepartureTime does not throw 'Ill.Arg.Exc.' on correct departure time")
    void getDepartureTimePositive() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      assertEquals(departureTime, register.getTrainDeparturesTime(trainNumber));
    }
  }

  @Nested
  @DisplayName("Negative tests for the TrainDepartureRegister class.")
  class NegativeTrainDepartureRegister {

    @Test
    @DisplayName("GetTrainDepartures does throw 'Ill.Arg.Exc.' on train departures.")
    void getTrainDeparturesPositive() {
      try {
        TrainDeparture departure = new TrainDeparture(
            LocalTime.of(17, 0), line, trainNumber, destination, delay, track);
        TrainDeparture departure1 = new TrainDeparture(
            LocalTime.of(15, 30), line, "1", destination, delay, track);
        register.addTrainDeparture(departure);
        register.addTrainDeparture(departure1);
        List<TrainDeparture> departures = register.getTrainDeparturesSorted();
        assertEquals(departure, departures.get(0), "Departure should be in index 1");
        assertEquals(departure1, departures.get(1), "Departure1 should be in index 0");

        fail("The method 'getTrainDeparturesPositive' did not throw on wrong sorted departures as expected.");
      } catch (AssertionError e) {
        assertEquals("Departure should be in index 1 ==> expected: <| 17:00          | L4      | 150            | Oslo           | 00:05 -> Expected departure: 17:05          |       |> but was: <| 15:30          | L4      | 1              | Oslo           | 00:05 -> Expected departure: 15:35          |       |>", e.getMessage());
      }
    }

    @Test
    @DisplayName("RemoveIfDeparted throws 'Ill.Arg.Exc.' on train departures removed when not supposed to be removed.")
    void removeIfDepartedNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            LocalTime.of(17, 0), line, trainNumber, destination, delay, track);
        TrainDeparture departure1 = new TrainDeparture(
            LocalTime.of(15, 30), line, "1", destination, delay, track);
        register.addTrainDeparture(departure);
        register.addTrainDeparture(departure1);
        LocalTime time = LocalTime.of(16, 0);
        register.removeIfDeparted(time);

        List<TrainDeparture> remainingDepartures = register.getTrainDeparturesSorted();
        assertEquals(2, remainingDepartures.size(), "No departures should be removed.");
        assertTrue(remainingDepartures.contains(departure), "Departure is in the list.");
        assertFalse(remainingDepartures.contains(departure1), "Departure1 is not in the list.");
        fail("The method 'removeIfDepartedNegative' did not throw on wrongful removed departures as expected.");
      } catch (AssertionError e) {
        assertEquals("No departures should be removed. ==> expected: <2> but was: <1>", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture throws 'Ill.Arg.Exc.' on train departure with blank line.")
    void addTrainDepartureThrowsOnBlankLine() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, "", trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);
        fail("The method 'addTrainDepartureThrowsOnBlankLine' did not throw on blank line.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Line' was a blank string. Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture throws 'Ill.Arg.Exc.' on train departure with blank train number.")
    void addTrainDepartureThrowsOnBlankTrainNumber() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, "", destination, delay, track);
        register.addTrainDeparture(departure);
        fail("The method 'addTrainDepartureThrowsOnBlankTrainNumber' did not throw on blank train number.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' was a blank string. Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture throws 'Ill.Arg.Exc.' on train departure with blank destination.")
    void addTrainDepartureThrowsOnBlankDestination() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, "", delay, track);
        register.addTrainDeparture(departure);
        fail("The method 'addTrainDepartureThrowsOnBlankDestination' did not throw on blank destination.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Destination' was a blank string. Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture throws 'Ill.Arg.Exc.' on invalid train number.")
    void addTrainDepartureThrowsOnInvalidTrainNumber() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, "100L", destination, delay, track);
        register.addTrainDeparture(departure);
        fail("The method 'addTrainDepartureThrowsOnInvalidTrainNumber' did not throw on"
            + " train number containing letters or symbols.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Train number' can only contain positive integer numbers.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture throws 'Ill.Arg.Exc.' on invalid destination.")
    void addTrainDepartureThrowsOnInvalidDestination() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, "Trondheim1", delay, track);
        register.addTrainDeparture(departure);
        fail("The method 'addTrainDepartureThrowsOnInvalidDestination' did not throw on"
            + " destination containing numbers or symbols.");
      } catch (IllegalArgumentException e) {
        assertEquals("The string for the parameter 'Destination' can only contain letters. Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AddTrainDeparture throws 'Ill.Arg.Exc.' on invalid track.")
    void addTrainDepartureThrowsOnInvalidTrack() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, -2);
        register.addTrainDeparture(departure);
        fail("The method 'addTrainDepartureThrowsOnInvalidDestination' did not throw on"
            + " invalid track.");
      } catch (IllegalArgumentException e) {
        assertEquals("The value for the parameter 'Track' was below 1 or above 10. Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("RemoveTrainDeparture throws 'Ill.Arg.Exc.' on attempt to remove train departure"
        + "not present.")
    void removeTrainDepartureNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.removeTrainDeparture("111");

        fail("The method 'removeTrainDepartureNegative' did not throw on attempted removal of"
            + " train departure not present.");
      } catch (IllegalArgumentException e) {
        assertEquals("The train number could not be found in the register.", e.getMessage());
      }
    }

    @Test
    @DisplayName("SearchForTrainNumber throws 'Ill.Arg.Exc.' on search for train departure not present.")
    void searchForTrainNumberNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.searchForTrainNumber("111");

        fail("The method 'searchForTrainNumberNegative' did not throw on search for train number"
            + " not present.");
      } catch (IllegalArgumentException e) {
        assertEquals("The train number could not be found in the register.", e.getMessage());
      }
    }

    @Test
    @DisplayName("SearchForDestination throws 'Ill.Arg.Exc.' on search for destination not present.")
    void searchForDestinationNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.searchForDestination("Madrid");

        fail("The method 'searchForDestinationNegative' did not throw on search for destination"
            + " not present.");
      } catch (IllegalArgumentException e) {
        assertEquals("The destination could not be found in the register.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AdjustDelay throws 'Ill.Arg.Exc.' on train number not present.")
    void adjustDelayNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.adjustDelay("111", LocalTime.of(0, 30));

        fail("The method 'adjustDelayNegative' did not throw on adjusting delay for train number"
            + " not present.");
      } catch (IllegalArgumentException e) {
        assertEquals("The train number could not be found in the register.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AdjustDelay throws 'Ill.Arg.Exc.' on invalid delay.")
    void adjustDelayNegative1() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.adjustDelay("100", LocalTime.of(24, 30));

        fail("The method 'adjustDelayNegative' did not throw on adjusting invalid delay");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): 24", e.getMessage());
      }
    }

    @Test
    @DisplayName("AssignTrack throws 'Ill.Arg.Exc.' on invalid train number.")
    void assignTrackNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.assignTrack("111", -1);

        fail("The method 'assignTrackNegative' did not throw on assigning track for train number"
            + " not present.");
      } catch (IllegalArgumentException e) {
        assertEquals("The train number could not be found in the register.", e.getMessage());
      }
    }

    @Test
    @DisplayName("AssignTrack throws 'Ill.Arg.Exc.' on invalid track.")
    void assignTrackNegative1() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);

        register.assignTrack(trainNumber, -2);

        fail("The method 'assignTrackNegative1' did not throw on assigning invalid track.");
      } catch (IllegalArgumentException e) {
        assertEquals("The value for the parameter 'Track' was below 1 or above 10. Please retry registration.", e.getMessage());
      }
    }

    @Test
    @DisplayName("TrainNumberExists returns false on train number that do not exist.")
    void trainNumberExistsNegative() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      boolean existingTrainNumber = register.trainNumberExists("22");
      if (existingTrainNumber) {
        fail("The method 'trainNumberExistsNegative' failed since it did not throw on existing train number.");
      }
      assertFalse(existingTrainNumber);
    }

    @Test
    @DisplayName("DestinationExists returns false on destination that do not exist.")
    void destinationExistsNegative() {
      TrainDeparture departure = new TrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      register.addTrainDeparture(departure);
      boolean existingDestination = register.destinationExists("Madrid");
      if (existingDestination) {
        fail("The method 'destinationExistsNegative' failed since it did find the existing destination.");
      }
      assertFalse(existingDestination);
    }

    @Test
    @DisplayName("VerifyUniqueTrainNumber throws on not unique train number.")
    void verifyUniqueTrainNumberNegative() {
      try {
        TrainDeparture departure = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure);
        TrainDeparture departure1 = new TrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
        register.addTrainDeparture(departure1);
        fail("The method 'verifyUniqueTrainNumberNegative' failed since it did not throw 'Ill.Arg.Exc.' "
            + "on adding a departure with a already existing train number.");
      } catch (IllegalArgumentException e) {
        assertEquals("Two or more train departures have the train number 150. Each train number should be unique. Please try again.", e.getMessage());
      }
    }

  }
}
