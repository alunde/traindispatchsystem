package edu.ntnu.stud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

public class ClockTest {
  @Nested
  @DisplayName("Positive tests for the Clock class.")

  class PositiveClockTest {

    @Test
    @DisplayName("Clock constructor does not throw 'Ill.Arg.Exc.' on valid input.")
    void clockConstructorPositive() {
      try {
        Clock clock = new Clock();
        assertEquals(LocalTime.of(0, 0), clock.getLastSetTime());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("GetLastSetTime does not throw 'Ill.Arg.Exc' on correct time.")
    void getLastSetTimePositive() {
      try {
        Clock clock = new Clock();
        assertEquals(LocalTime.of(0, 0), clock.getLastSetTime());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception message: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("SetTime does not throw 'Ill.Arg.Exc' on valid time.")
    void setTimePositive() {
      try {
        Clock clock = new Clock();
        clock.setTime(LocalTime.of(12, 0));
        assertEquals(LocalTime.of(12, 0), clock.getLastSetTime());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception message: " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests for the Clock class.")

  class NegativeClockTest {

    @Test
    @DisplayName("SetTime throws 'Ill.Arg.Exc' on invalid time.")
    void setTimeNegative() {
      try {
        Clock clock = new Clock();
        LocalTime time = LocalTime.of(24, 0);
        clock.setTime(time);
        fail("The test failed since the correct time was given.");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): 24", e.getMessage());
      }
    }
  }
}
