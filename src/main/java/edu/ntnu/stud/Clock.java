package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * The Clock class represents a simple clock with the ability to set and
 * retrieve the last set time.
 *
 * @author 10018
 * @version 1.0
 * @since 0.1
 */
public class Clock {
  private LocalTime lastSetTime;

  /**
   * Constructs a new Clock object with the initial last set time to (00:00).
   */
  public Clock() {
    this.lastSetTime = LocalTime.of(0, 0);
  }

  /**
   * Gets the last set time of the clock.
   *
   * @return The last set time.
   */
  public LocalTime getLastSetTime() {
    return lastSetTime;
  }

  /**
   * Sets the time of day based on user input from the scanner. If the time entered
   * is before the last set time, the user is given another chance until a valid
   * time is entered. If the time is valid, the lastSetTime is updated.
   *
   * @param time The time
   */
  public void setTime(LocalTime time) {
    this.lastSetTime = time;
  }
}
