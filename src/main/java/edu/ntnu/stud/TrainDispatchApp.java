package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application, that initializes
 * the user interface and starts the application.
 *
 * @author 10018
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {

  /**
   * The main method that runs the application.
   *
   * @param args The arguments for the main method (not used in this application).
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();

    try {
      ui.init();
      ui.start();
    } catch (Exception e) {
      System.out.println("Something wrong happened: " + e.getMessage());
    }
  }
}
