package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Represents information about a train departure.
 * This class provides details such as departure time, line, train number,
 * destination, delay and track number
 *
 * @author 10018
 * @version 1.0
 * @since 0.1
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final String trainNumber;
  private final String destination;
  private LocalTime delay;
  private int track;

  /**
   * Verifies a given track for its integer number. The check fails is the integer is outside
   * the interval between 1 and 10.
   *
   * @param track is the track that is being verified.
   * @throws IllegalArgumentException if the integer given as track is outside the interval
   *                                  between 1 and 10.
   */
  private void verifyTrack(int track, String parameter)
      throws IllegalArgumentException {
    if (track < -1 || track > 10 || track == 0) {
      throw new IllegalArgumentException("The value for the parameter '" + parameter
          + "' was below 1 or above 10. Please retry registration.");
    }
  }

  /**
   * Verifies a given destination that it only contains letters. The check fails
   * if the destination contains other symbols and/or numbers.
   *
   * @param destination is the destination that is being verified.
   * @throws IllegalArgumentException if the string given as destination contains
   *                                  symbols or numbers.
   */
  private void verifyString(String destination, String parameter)
      throws IllegalArgumentException {
    if (!destination.chars().allMatch(Character::isLetter)) {
      throw new IllegalArgumentException("The string for the parameter '" + parameter
          + "' can only contain letters. Please retry registration.");
    }
  }

  /**
   * Verifies a given String for its containing information. The check fails if the
   * string is blank.
   *
   * @param string is the string being verified.
   * @param parameter is the name of the String being verified. Used in the exception message
   *                      if the Ill.Arg.Exc is being thrown.
   * @throws IllegalArgumentException if the String is either "" or consists only of
   *                                  whitespace, as it does not give any information.
   */
  private void verifyNotEmpty(String string, String parameter)
      throws IllegalArgumentException {
    if (string.isBlank()) {
      throw new IllegalArgumentException("The string for the parameter '" + parameter
      + "' was a blank string. Please retry registration.");
    }
  }

  /**
   * Verifies a given train number that it only consists of integer numbers. The check fails
   * if the string contains symbols and/or letters.
   *
   * @param trainNumber is the train number being verified.
   * @throws IllegalArgumentException if the String given contains symbols and/or
   *                                  letters.
   */
  private void verfifyStringInteger(String trainNumber, String parameter)
      throws IllegalArgumentException {
    if (!trainNumber.chars().allMatch(Character::isDigit)) {
      throw new IllegalArgumentException("The string for the parameter '" + parameter
          + "' can only contain positive integer numbers.");
    }
  }

  /**
   * Verifies that the length of a string does not exceed the maximum length.
   *
   * @param string The string to be verified.
   * @param parameter The name of the parameter associated with the string.
   * @throws IllegalArgumentException If the string exceeds the maximum allowed string.
   */
  private void verifyStringLength(String string, String parameter)
      throws IllegalArgumentException {
    if (!(string.length() <= 5)) {
      throw new IllegalArgumentException("The string for the parameter '" + parameter
            + "' can maximum have 5 characters.");
    }
  }

  /**
   * Verifies that the string only contains letters and numbers.
   *
   * @param string The string to be verified.
   * @param parameter The name of the parameter associated with the string.
   * @throws IllegalArgumentException If the string does not only contain letters and numbers.
   */
  private void verifyStringLettersAndNumbers(String string, String parameter)
      throws IllegalArgumentException {
    if (!string.chars().allMatch(c -> Character.isLetterOrDigit((char) c))) {
      throw new IllegalArgumentException("The string for the parameter '" + parameter
          + "' can only contain letters and numbers.");
    }
  }

  /**
   * Verifies that the time is not null.
   *
   * @param time The time to be verified.
   * @param parameterName The name of the parameter associated with the time.
   * @throws IllegalArgumentException If the time is null.
   */
  private void verifyTime(LocalTime time, String parameterName)
      throws IllegalArgumentException {
    if (time == null) {
      throw new IllegalArgumentException(parameterName + " cannot be null");
    }
  }

  /**
   * Constructs a train departure object.
   *
   * @param departureTime The departure time of the train
   * @param line          The line of the train
   * @param trainNumber   The train number of the train
   * @param destination   The destination of the train
   * @param delay         The delay of the train
   * @param track         The track of the train
   */
  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, LocalTime delay, int track)
      throws IllegalArgumentException {
    verifyNotEmpty(line, "Line");
    verifyNotEmpty(trainNumber, "Train number");
    verifyNotEmpty(destination, "Destination");
    verfifyStringInteger(trainNumber, "Train number");
    verifyString(destination, "Destination");
    verifyTrack(track, "Track");
    verifyStringLength(line, "Line");
    verifyStringLength(trainNumber, "Train number");
    verifyStringLettersAndNumbers(line, "Line");
    verifyTime(delay, "Delay");

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    setTrack(track);
    setDelay(delay);
  }

  /**
   * Gets the departure time of the train departure.
   *
   * @return The departure time
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Gets the line of the train departure.
   *
   * @return The line
   */
  public String getLine() {
    return line;
  }

  /**
   * Gets the train number of the train departure.
   *
   * @return The train number
   */
  public String getTrainNumber() {
    return trainNumber;
  }

  /**
   * Gets the destination of the train departure.
   *
   * @return The destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Gets the delay of the train departure.
   *
   * @return The delay
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Sets a delay or changes the current delay of the train departure.
   * Checks if the delay's hour exceeds the maxHours, or if the delay's hour is the same
   * as maxHours, but the delay's minutes exceeds the maxMinutes.
   *
   * @param delay The delay to be set.
   * @throws IllegalArgumentException if the given delay exceeds the maximum allowed delay.
   *        Which is 23:59 this day.
   */
  public void setDelay(LocalTime delay) {
    verifyTime(delay, "Delay");

    int maxHours = 23 - departureTime.getHour();
    int maxMinutes = 59 - departureTime.getMinute();

    //Checks if the delay's hour exceeds the maxHours, or if the delay's hour is the same
    // as maxHours, but the delay's minutes exceeds the maxMinutes.
    if (delay.getHour() > maxHours
        || (delay.getHour() == maxHours && delay.getMinute() > maxMinutes)) {
      throw new IllegalArgumentException("Departure time plus delay must be at current day.");
    }
    this.delay = delay;
  }

  /**
   * Gets the track number for the train departure.
   *
   * @return The track
   */
  public int getTrack() {
    return track;
  }

  /**
   * Sets a track or changes the current track.
   *
   * @param track The track to be set
   */
  public void setTrack(int track) {
    verifyTrack(track, "Track");
    this.track = track;
  }

  /**
   * A method that adds the departure time with the delay to a variable
   * called adjustedDepartureTIme. To ensure that the adjustedDepartureTime is not interpret
   * as departing earlier than the departureTime, as delay can be up to 23 hours
   * and 59 minutes. The adjustedDepartureTime is being set to 23:59 if
   * adjustedDepartureTime is before departureTime.
   *
   * @return The adjusted departure time.
   */
  public LocalTime getAdjustedDepartureTime() {
    return this.departureTime.plusHours(this.delay.getHour())
        .plusMinutes(this.delay.getMinute());
  }

  /**
   * Returns a string of the train departure.
   *
   * @return A String with information about the train departure
   */
  @Override
  public String toString() {
    return String.format("| %-15s| %-8s| %-15s| %-15s| %-44s| %-6s|",
        departureTime,
        line,
        trainNumber,
        destination,
        ((delay.equals(LocalTime.MIDNIGHT)) ? "" : delay + " -> Expected departure: "
            + (getAdjustedDepartureTime())),
        ((track == -1) ? "" : track));
  }
}
