package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.InputMismatchException;

/**
 * This class represents the user interface for the train dispatch application.
 *
 *  <p>The UserInterface class provides a command-line interface for managing train departures,
 *  allowing the user to display, add, remove, search and modify the train departures.</p>
 *
 *  <p>The class interacts with the TrainDepartureRegister to handle the train departures,
 *  the Clock class to change the time and the Scan class to gather user input.
 *
 *  @author 10018
 *  @version 1.0
 *  @since 0.1
 */
public class UserInterface {
  //Constants representing menu options.
  private static final int DISPLAY_BOARD = 1;
  private static final int ADD_TRAIN_DEPARTURE = 2;
  private static final int REMOVE_TRAIN_DEPARTURE = 3;
  private static final int SEARCH_FOR_TRAIN_NUMBER = 4;
  private static final int SEARCH_FOR_DESTINATION = 5;
  private static final int ADJUST_DELAY = 6;
  private static final int ASSIGN_TRACK = 7;
  private static final int SET_TIME = 8;
  private static final int EXIT = 9;

  //Visualization strings for the output.
  private final String visualLine = "|" + "-".repeat(114) + "|";
  private final String visualLine2 = "-".repeat(116);
  private final String displayOverview = String.format("| %-15s| %-8s| %-15s| %-15s| %-44s| %-6s|",
      "Departure time",
      "Line",
      "Train number",
      "Destination",
      "Delay",
      "Track");
  private static final String PLEASE_TRY_AGAIN = "Please try again:";

  //Instances
  private TrainDepartureRegister trainDepartureRegister;
  private Clock clock;
  private Scan scan;


  /**
   * Displays the main menu and reads the users menu choice.
   *
   * @return The user's menu choice.
   */
  private int showMenu() {
    System.out.println(visualLine2);
    System.out.println("MAIN MENU");
    System.out.println("1. Show all train departures");
    System.out.println("2. Add a train departure");
    System.out.println("3. Remove a train departure");
    System.out.println("4. Search for a train number");
    System.out.println("5. Search for a destination");
    System.out.println("6. Adjust the delay to a train departure");
    System.out.println("7. Choose a track for a train departure");
    System.out.println("8. Set time of day");
    System.out.println("9. Exit");
    int menuChoice = scan.scanMenuOption();
    System.out.println(visualLine2);

    return menuChoice;
  }

  /**
   * Initializes the user interface by creating instances of TrainDepartureRegister, Clock,
   * the Scanner and adding some initial train departures.
   */
  public void init() {
    trainDepartureRegister = new TrainDepartureRegister();
    clock = new Clock();
    scan = new Scan();

    TrainDeparture departure = new TrainDeparture(
        LocalTime.of(20, 51), "L4000", "100", "Oslo",
        LocalTime.of(3, 8), -1);
    TrainDeparture departure1 = new TrainDeparture(
        LocalTime.of(10, 16), "R8", "10", "Trondheim",
        LocalTime.of(0, 5), 9);
    TrainDeparture departure2 = new TrainDeparture(
        LocalTime.of(7, 0), "9", "111", "Trondheim",
        LocalTime.of(0, 5), 1);
    TrainDeparture departure3 = new TrainDeparture(
        LocalTime.of(16, 45), "LN1", "62", "Stockholm",
        LocalTime.of(0, 5), -1);
    TrainDeparture departure4 = new TrainDeparture(
        LocalTime.of(16, 59), "VY7", "59", "Bergen",
        LocalTime.of(0, 5), 10);

    trainDepartureRegister.addTrainDeparture(departure);
    trainDepartureRegister.addTrainDeparture(departure1);
    trainDepartureRegister.addTrainDeparture(departure2);
    trainDepartureRegister.addTrainDeparture(departure3);
    trainDepartureRegister.addTrainDeparture(departure4);
  }

  /**
   * Starts the main loop of the user interface, allowing the user to interact with
   * the train dispatch application through the command-line menu.
   */
  public void start() {
    boolean run = true;
    while (run) {
      try {
        int menuChoice = this.showMenu();
        switch (menuChoice) {
          case DISPLAY_BOARD -> showAllTrainDepartures();
          case ADD_TRAIN_DEPARTURE -> {
            System.out.println("ADD TRAIN DEPARTURE");
            addTrainDeparture();
          }
          case REMOVE_TRAIN_DEPARTURE -> {
            System.out.println("REMOVE TRAIN DEPARTURE");
            removeTrainDeparture();
          }
          case SEARCH_FOR_TRAIN_NUMBER -> {
            System.out.println("SEARCH FOR TRAIN NUMBER");
            searchForTrainNumber();
          }
          case SEARCH_FOR_DESTINATION -> {
            System.out.println("SEARCH FOR DESTINATION");
            searchForDestination();
          }
          case ADJUST_DELAY -> {
            System.out.println("ADJUST DELAY");
            adjustDelay();
          }
          case ASSIGN_TRACK -> {
            System.out.println("ASSIGN TRACK");
            assignTrack();
          }
          case SET_TIME -> {
            System.out.println("SET TIME");
            setTime();
          }
          case EXIT -> {
            run = false;
            System.out.println("Exiting...");
          }
          default -> System.out.println("Invalid number. "
              + "Please enter a integer number between 1 and 9.");
        }
      } catch (InputMismatchException e) {
        System.out.println("Invalid input! Please enter a integer number between 1 and 9");
      }
    }
  }

  /**
   * Displays all the train departures in the console, filtering out those
   * that have already departed based on the set time.
   */
  private void showAllTrainDepartures() {
    System.out.printf("|%43sWelcome to NTNU train station%42s|%n", "", "");
    System.out.printf("|%57s%57s|%n", clock.getLastSetTime(), "");
    System.out.println(visualLine);
    System.out.println(displayOverview);
    System.out.println(visualLine);
    trainDepartureRegister.removeIfDeparted(clock.getLastSetTime());
    trainDepartureRegister.getTrainDeparturesSorted().forEach(System.out::println);
  }

  /**
   * Adds a new train departure to the train departure register based on user input.
   * The user is prompted to enter information such as departure time, line, train number,
   * destination, delay and track.
   *
   * <p>The method ensures that the entered train number does not already exist. The user
   * is prompted to choose 'yes' or 'no' on adding delay or track. If 'no' is entered
   * to delay, the delay is set to 00:00 and if 'no' is entered for track, the track
   * is set to -1.
   *
   * <p>User guidance is given for wrong input and for valid information added.
   */

  private void addTrainDeparture() {
    //Enter departure time to the new departure.
    System.out.println("Enter departure time:");
    final LocalTime addDepartureTime = scan.scanTime();
    System.out.println("Departure time added to the departure.");
    System.out.println(visualLine2);

    //Enter line to the new departure.
    System.out.println("Enter line:");
    final String addLine = scan.scanLine();
    System.out.println("Line added to the departure.");
    System.out.println(visualLine2);

    //Enter train number to the new departure.
    System.out.println("Enter train number:");
    boolean validTrainNumber = false;
    String addTrainNumber = "";
    while (!validTrainNumber) {
      addTrainNumber = scan.scanTrainNumber();
      //Checks if added train number already exist.
      if (trainDepartureRegister.trainNumberExists(addTrainNumber)) {
        System.out.println("Train number already exists.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validTrainNumber = true;
        System.out.println("Train number added to the departure.");
        System.out.println(visualLine2);
      }
    }

    //Enter destination to the new departure.
    System.out.println("Enter destination:");
    final String addDestination = scan.scanDestination();
    System.out.println("Destination added to the departure.");
    System.out.println(visualLine2);


    //Enter delay to the new departure. If 'no' LocalTime is set 00:00,
    //if 'yes' user adds delay
    LocalTime addDelay;
    if (scan.yesNoSwitch("delay")) {
      System.out.println("Enter delay:");
      addDelay = scan.scanDelay(addDepartureTime);
      System.out.println("Delay added to the departure.");
      System.out.println(visualLine2);
    } else {
      addDelay = LocalTime.of(0, 0);
    }

    //Enter track to the new departure. If 'no' track is set to -1 is added,
    // if 'yes' user adds track.
    int addTrack;
    if (scan.yesNoSwitch("track")) {
      System.out.println("Enter track:");
      addTrack = scan.scanTrack();
      System.out.println("Track added to the departure.");
      System.out.println(visualLine2);
    } else {
      addTrack = -1;
    }

    //Adds new departure based on the earlier entered input.
    TrainDeparture newTrainDeparture = new TrainDeparture(addDepartureTime,
        addLine, addTrainNumber, addDestination, addDelay, addTrack);
    trainDepartureRegister.addTrainDeparture(newTrainDeparture);
    System.out.println(visualLine2);
    System.out.println("TRAIN DEPARTURE ADDED.");
  }

  /**
   * Removes a train departure from the train departure register based on the
   * entered train number. The user is prompted to enter a train number, and
   * the method validates if the train number is present in the train departure
   * register. If the train number is present, the train departure will be removed
   * If not, the user is given another chance until present train
   * number is given.
   *
   */
  private void removeTrainDeparture() {
    String trainNumber;
    boolean validTrainNumber = false;
    while (!validTrainNumber) {
      trainNumber = scan.scanTrainNumber();
      //Checks that train number exists
      if (!trainDepartureRegister.trainNumberExists(trainNumber)) {
        System.out.println("Train number do not exists.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validTrainNumber = true;
        trainDepartureRegister.removeTrainDeparture(trainNumber);
        System.out.println("TRAIN DEPARTURE REMOVED.");
      }
    }
  }

  /**
   * Searches for a train departure in the train departure register based on the
   * entered train number. The user is prompted to enter a train number, and the
   * method checks if the train number is present in the train departure register.
   * If the train number is present, the information about the train departure
   * corresponding to the train number will be printed. If the train number is
   * not present, the user will get another chance to enter a train number
   * until present train number is entered.
   */
  private void searchForTrainNumber() {
    String trainNumber;
    boolean validTrainNumber = false;
    while (!validTrainNumber) {
      trainNumber = scan.scanTrainNumber();
      //Checks that train number exists
      if (!trainDepartureRegister.trainNumberExists(trainNumber)) {
        System.out.println("Train number do not exists.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validTrainNumber = true;
        System.out.println(visualLine2);
        System.out.println(displayOverview);
        System.out.println(visualLine);
        System.out.println(trainDepartureRegister.searchForTrainNumber(trainNumber));
      }
    }
  }

  /**
   * Searches for a train departure in the train departure register based on the
   * entered destination. The user is prompted to enter a destination, and the
   * method checks if the destination is present in the train departure register.
   * If the destination is present, the information about the train departures
   * corresponding to the destination will be printed. If the destination is
   * not present, the user will get another chance to enter a destination
   * until present destination is entered.
   */
  private void searchForDestination() {
    String destination;
    boolean validDestination = false;
    while (!validDestination) {
      destination = scan.scanDestination();
      //Checks that the destination exists
      if (!trainDepartureRegister.destinationExists(destination)) {
        System.out.println("Destination do not exist.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validDestination = true;
        System.out.println(visualLine2);
        System.out.println(displayOverview);
        System.out.println(visualLine);
        trainDepartureRegister.searchForDestination(destination)
            .forEach(System.out::println);
      }
    }
  }

  /**
   * Adjusts the delay for a train departure based on the entered train number.
   * The user is prompted to enter a train number corresponding to a train departure,
   * that the user wants to adjust delay for. If the train number is not present in
   * the train departure register, the user is given a new chance to enter a train
   * number until a present train number is given. Then the user is prompted to
   * enter the adjusted delay.
   */
  private void adjustDelay() {
    System.out.println("Enter train number:");
    String trainNumber = "";
    boolean validTrainNumber = false;
    while (!validTrainNumber) {
      trainNumber = scan.scanTrainNumber();
      //Checks that the train number exists
      if (!trainDepartureRegister.trainNumberExists(trainNumber)) {
        System.out.println("Train number do not exists.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validTrainNumber = true;
      }
    }
    System.out.println("Enter the adjusted delay:");
    LocalTime delay = scan.scanDelay(trainDepartureRegister.getTrainDeparturesTime(trainNumber));
    trainDepartureRegister.adjustDelay(trainNumber, delay);
    System.out.println("DELAY ADJUSTED.");
  }

  /**
   * Assign a track for a train departure based on the entered train number.
   * The user is prompted to enter a train number corresponding to a train departure,
   * that the user wants to assign a track for. If the train number is not present in
   * the train departure register, the user is given a new chance to enter a train
   * number until a present train number is given. Then the user is prompted to
   * enter the new track.
   */
  private void assignTrack() {
    System.out.println("Enter train number:");
    String trainNumber = "";
    boolean validTrainNumber = false;
    while (!validTrainNumber) {
      trainNumber = scan.scanTrainNumber();
      //Checks that the train number exists
      if (!trainDepartureRegister.trainNumberExists(trainNumber)) {
        System.out.println("Train number do not exists.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validTrainNumber = true;
      }
    }
    System.out.println("Enter the track:");
    int track = scan.scanTrack();
    trainDepartureRegister.assignTrack(trainNumber, track);
    System.out.println("TRACK ASSIGNED.");
  }

  /**
   * Sets the time of day for the application based on user input. The user is
   * prompted to add a time of day in the format (hh:mm). The entered time
   * is then used to update the clock.
   */
  private void setTime() {
    System.out.println("Enter the time of day in the format (hh:mm), maximum 23:59.");
    clock.setTime(scan.scanTime());
    System.out.println("TIME UPDATED.");
  }
}
