package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class is a register to manage train departures, and methods to show different types
 * og information to the user.
 *
 * @author 10018
 * @version 1.0
 * @since 0.1
 */

public class TrainDepartureRegister {
  private final List<TrainDeparture> trainDepartures = new ArrayList<>();

  /**
   * A methode that sorts all the train departure after departure time,
   * where the first to leave is on top.
   *
   * @return A list of the train departures sorted after departure time
   */
  public List<TrainDeparture> getTrainDeparturesSorted() {
    return trainDepartures.stream()
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .toList();
  }

  /**
   * Removed the train departures from the register that have already departed. Based on the
   * provided time.
   *
   * @param time The reference time used to filter out train departures that have already departed.
   */
  public void removeIfDeparted(LocalTime time) {
    trainDepartures.removeIf(trainDeparture ->
        trainDeparture.getAdjustedDepartureTime().isBefore(time));
  }

  /**
   * A method for adding a train departure to the register.
   *
   * @param departure The train departure that is being added, with departure time, line,
   *                  train number, destination, delay and track
   */

  public void addTrainDeparture(TrainDeparture departure) {
    verifyUniqueTrainNumberAdded(departure);
    trainDepartures.add(departure);
  }

  /**
   * A method for removing a train departure from the register.
   *
   * @param trainNumber The train number of the train departure that is being removed
   */

  public void removeTrainDeparture(String trainNumber) throws IllegalArgumentException {
    verifyTrainNumberPresence(trainNumber);
    trainDepartures.removeIf(trainDeparture ->
        trainDeparture.getTrainNumber().equalsIgnoreCase(trainNumber)
    );
  }

  /**
   * A method that searches for a train departure with a specific train number.
   *
   * @param trainNumber The train number that is being searched for.
   * @return A list with the information about the specified train number.
   */

  public TrainDeparture searchForTrainNumber(String trainNumber) throws IllegalArgumentException {
    verifyTrainNumberPresence(trainNumber);
    return trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getTrainNumber()
            .equalsIgnoreCase(trainNumber))
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException(
            "No train departure found with the train number " + trainNumber));
  }

  /**
   * A method that searches for train departures that have a specific destination.
   *
   * @param destination The destination that is being searched for.
   * @return A list of train departures with the specified destination.
   */

  public List<TrainDeparture> searchForDestination(String destination)
      throws IllegalArgumentException {
    verifyDestinationPresence(destination);
    return trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getDestination()
            .equalsIgnoreCase(destination))
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .toList();

  }

  /**
   * A method to give or adjust a train departures delay.
   *
   * @param trainNumber The train number of the train departures delay being adjusted.
   * @param newDelay    The new delay that a train departure is given.
   */
  public void adjustDelay(String trainNumber, LocalTime newDelay) throws IllegalArgumentException {
    verifyTrainNumberPresence(trainNumber);
    trainDepartures.stream()
        .filter(departure -> departure.getTrainNumber().equals(trainNumber))
        .findFirst()
        .ifPresent(departure -> departure.setDelay(newDelay));
  }

  /**
   * A method to assign or change a track for a train departure.
   *
   * @param trainNumber The train number of the train departures track being chosen.
   * @param newTrack    The new track to be assigned to a train departure.
   */
  public void assignTrack(String trainNumber, int newTrack) throws IllegalArgumentException {
    verifyTrainNumberPresence(trainNumber);
    trainDepartures.stream()
        .filter(departure -> departure.getTrainNumber().equals(trainNumber))
        .findFirst()
        .ifPresent(departure -> departure.setTrack(newTrack));
  }

  /**
   * Retrieves the departure time of a train departure with a specific train number.
   *
   * @param trainNumber The train number representing the train departure.
   * @return The departure time of the train departure with the specific train number.
   * @throws IllegalArgumentException if no train departure is found with the provided train number.
   */
  public LocalTime getTrainDeparturesTime(String trainNumber) {
    return trainDepartures.stream()
        .filter(departure -> departure.getTrainNumber().equals(trainNumber))
        .findFirst()
        .map(TrainDeparture::getDepartureTime)
        .orElseThrow(() -> new IllegalArgumentException("Train number not found: " + trainNumber));
  }

  /**
   * A method to verify that the train number being handled is present.
   *
   * @param trainNumber The train number being checked for its presence.
   * @throws IllegalArgumentException If the train number is not present.
   */
  private void verifyTrainNumberPresence(String trainNumber) throws IllegalArgumentException {
    if (!trainNumberExists(trainNumber)) {
      throw new IllegalArgumentException("The train number could not be found in the register.");
    }
  }

  /**
   * A method to verify that the destination being handled is present.
   *
   * @param destination The destination being checked for its presence.
   * @throws IllegalArgumentException If the destination is not present.
   */
  private void verifyDestinationPresence(String destination)
      throws IllegalArgumentException {
    if (!destinationExists(destination)) {
      throw new IllegalArgumentException("The destination could not be found in the register.");
    }
  }

  /**
   * A method to verify that the train number added is unique.
   *
   * @param newDeparture The train number in newDeparture that is being checked for its uniqueness.
   * @throws IllegalArgumentException If the train number in newDeparture is not unique.
   */
  private void verifyUniqueTrainNumberAdded(TrainDeparture newDeparture)
      throws IllegalArgumentException {
    if (trainNumberExists(newDeparture.getTrainNumber())) {
      throw new IllegalArgumentException("Two or more train departures have the train number "
          + newDeparture.getTrainNumber() + ". Each train number should"
          + " be unique. Please try again.");
    }
  }

  /**
   * A method to ensure that a new train departure do not have the same
   * train number as a train departures that already registered and makes sure that
   * a train departure that is being handled actually exists.
   *
   * @param trainNumber The train number that is being checked.
   * @return true if the train number already exists,
   *        false train number is not taken.
   */
  public boolean trainNumberExists(String trainNumber) {
    return trainDepartures.stream()
        .anyMatch(departure -> departure.getTrainNumber().equalsIgnoreCase(trainNumber));
  }

  /**
   * A method to check if the given destination can be found in the arrayList trainDepartures.
   *
   * @param destination The destination given as an input, by the user.
   * @return true if the destination exists,
   *        false if the destination does not exist.
   */
  public boolean destinationExists(String destination) {
    return trainDepartures.stream()
        .anyMatch(departure -> departure.getDestination().equalsIgnoreCase(destination));
  }
}
