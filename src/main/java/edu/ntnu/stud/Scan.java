package edu.ntnu.stud;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The Scan class provides methods for scanning user input in the console.
 * It includes methods for scanning various types of input, such as train numbers, tracks,
 * departure times, delays, destinations, lines, menu choices, and yes/no switches.
 * It also includes utility methods for validating input based on specific criteria.
 *
 * @author 10018
 * @version 1.0
 * @since 0.1
 */
public class Scan {
  private static final String PLEASE_TRY_AGAIN = "Please try again:";

  /**
   * Allows the user to enter train number and validates a train number entered by the user.
   * Gives user guidance if the entered train number is not valid. Given new chance until
   * valid train number is entered.
   *
   * @return The scanned and validated train number.
   */
  protected String scanTrainNumber() {
    String trainNumber = "";
    Scanner scanner = new Scanner(System.in);
    boolean validTrainNumber = false;
    while (!validTrainNumber) {
      trainNumber = scanner.nextLine();
      if (validateNotEmpty(trainNumber) || !validateStringAsInteger(trainNumber)
          || !validateStringLength(trainNumber)) {
        System.out.println("Train number must be a positive integer number with max 5 numbers, ");
        System.out.println("and cannot be empty.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validTrainNumber = true;
      }
    }
    return trainNumber;
  }

  /**
   * Allows the user to enter track and validates a track entered by the user. Gives user guidance
   * if the entered track is not valid. Given new chance until valid track is entered.
   *
   * @return The entered and validated track.
   */
  protected int scanTrack() {
    int track = 0;
    Scanner scanner = new Scanner(System.in);
    boolean validTrack = false;
    while (!validTrack) {
      try {
        track = scanner.nextInt();
        if (!validateTrack(track)) {
          System.out.println("Track must be a integer between 1 and 10.");
          System.out.println(PLEASE_TRY_AGAIN);
        } else {
          validTrack = true;
        }
      } catch (InputMismatchException e) {
        System.out.println("Invalid input. Must be an integer between 1 and 10 ");
        System.out.println(PLEASE_TRY_AGAIN);
        scanner.nextLine();
      }
    }
    return track;
  }

  /**
   * Allows the user to enter a LocalTime and validates the LocalTime entered by the user.
   * Gives user guidance if the entered LocalTime is not valid. Given new chance until
   * valid LocalTime is entered.
   *
   * @return The entered and validated LocalTime.
   */
  protected LocalTime scanTime() {
    LocalTime localTime = null;
    Scanner scanner = new Scanner(System.in);
    while (localTime == null) {
      if (scanner.hasNextLine()) {
        String userInput = scanner.nextLine();
        try {
          localTime = LocalTime.parse(userInput);
        } catch (DateTimeException e) {
          System.out.println("Invalid departure time. 23 hours and 59 minutes is maximum, "
              + "in format (hh:mm).");
          System.out.println(PLEASE_TRY_AGAIN);
        }
      }
    }
    return localTime;
  }

  /**
   * Allows the user to enter a delay and validates the delay entered by the user.
   * Gives user guidance if the entered delay is not valid. Given new chance until
   * valid delay is entered.
   *
   * @return The entered and validated delay.
   */
  protected LocalTime scanDelay(LocalTime departureTime) {
    LocalTime delay = null;
    Scanner scanner = new Scanner(System.in);
    boolean validDelay = false;
    while (!validDelay) {
      try {
        String userInput = scanner.nextLine();
        delay = LocalTime.parse(userInput);
        if (validateAdjustedDepartureTime(departureTime, delay)) {
          System.out.println("Departure time plus delay must be at the current day.");
          System.out.println(PLEASE_TRY_AGAIN);
        } else {
          validDelay = true;
        }
      } catch (DateTimeException e) {
        System.out.println("Invalid departure time. 23 hours and 59 minutes is maximum, "
            + "in format (hh:mm).");
        System.out.println(PLEASE_TRY_AGAIN);
      }
    }
    return delay;
  }

  /**
   * Allows the user to enter a destination and validates the destination entered by the user.
   * Gives user guidance if the entered destination is not valid. Given new chance until
   * valid destination is entered.
   *
   * @return The entered and validated destination.
   */
  protected String scanDestination() {
    Scanner scanner = new Scanner(System.in);
    String destination = "";
    boolean validDestination = false;

    while (!validDestination) {
      destination = scanner.nextLine();

      if (validateNotEmpty(destination) || !validateLettersOnly(destination)) {
        System.out.println("Destination can only contain letters and cannot be empty.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validDestination = true;
      }
    }

    return destination;
  }

  /**
   * Allows the user to enter a line and validates the line entered by the user. Gives user guidance
   * if the entered line is not valid. Given new chance until valid line is entered.
   *
   * @return The entered and validated line.
   */
  protected String scanLine() {
    Scanner scanner = new Scanner(System.in);
    String line = "";
    boolean validLine = false;

    while (!validLine) {
      line = scanner.nextLine();

      if (validateNotEmpty(line) || !(validateLettersAndDigitsOnly(line)
          && validateStringLength(line))) {
        System.out.println("Line can only contain letters and numbers, max 5 characters");
        System.out.println("and cannot be empty.");
        System.out.println(PLEASE_TRY_AGAIN);
      } else {
        validLine = true;
      }
    }

    return line;
  }

  /**
   * Allows the user to enter an integer representing a menu option, and validates the integer
   * entered by the user. Gives user guidance if the entered integer is not valid. Given new
   * chance until valid integer is entered.
   *
   * @return The entered and validated integer representing a menu option.
   */
  protected int scanMenuOption() {
    int choice = 0;
    Scanner scanner = new Scanner(System.in);
    while (choice == 0) {
      System.out.println("Enter main menu option (integer between 1 and 9):");
      choice = scanner.nextInt();
      if (choice >= 1 && choice <= 9) {
        System.out.println("Exiting MAIN MENU...");
      } else {
        System.out.println("Invalid input. Option must be a integer between 1 and 9.");
        System.out.println(PLEASE_TRY_AGAIN);
      }
    }
    return choice;
  }

  /**
   * Allows the user to enter 'yes' or 'no' and validates the answer entered by the user.
   * Gives user guidance if the entered answer does not correspond to 'yes' or 'no'.
   * Given new chance until valid answer is entered.
   *
   * @return The entered and validated answer to the switch.
   */
  protected boolean yesNoSwitch(String parameterName) {
    boolean run = true;
    boolean result = false;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Do you want to add " + parameterName + "?");
    System.out.println("Enter 'yes' or 'no':");
    while (run) {
      try {
        String choice = scanner.nextLine().toLowerCase();
        switch (choice) {
          case "yes" -> {
            result = true;
            run = false;
          }
          case "no" -> run = false;

          default -> {
            System.out.println("Choice must be 'yes' or 'no'.");
            System.out.println(PLEASE_TRY_AGAIN);
          }
        }
      } catch (InputMismatchException e) {
        System.out.println("Invalid input. Choice must be 'yes' or 'no'.");
        System.out.println(PLEASE_TRY_AGAIN);
      }
    }
    return result;
  }

  /**
   * Validates whether the entered string is empty.
   *
   * @param string The string to be verified.
   * @return True if the string is empty.
   *         False otherwise.
   */
  public boolean validateNotEmpty(String string) {
    return string.isEmpty();
  }

  /**
   * Validates whether the entered string only consists of positive integers.
   *
   * @param string The string to be verified.
   * @return True if the string is not empty and only consists of digits.
   *        False otherwise.
   */
  public boolean validateStringAsInteger(String string) {
    return string.chars().allMatch(Character::isDigit);
  }

  /**
   * Validates whether the entered string consists of maximum 5 characters.
   *
   * @param string The string to be validated.
   * @return True if the string is not empty and has 5 or fewer characters.
   *        False otherwise.
   */
  public boolean validateStringLength(String string) {
    return string.length() <= 5;
  }

  /**
   * Validates whether the entered string only consists of letters.
   *
   * @param string The string entered to be validated.
   * @return True if the string is not empty and only consists of letters.
   *        False otherwise.
   */
  public boolean validateLettersOnly(String string) {
    return string.chars().allMatch(Character::isLetter);
  }

  /**
   * Validates whether the entered string only consists of letters and digits.
   *
   * @param string The string entered to be validated.
   * @return True if the string is not empty and only consists of letters.
   *        False otherwise.
   */
  public boolean validateLettersAndDigitsOnly(String string) {
    return string.chars().allMatch(c -> Character.isLetterOrDigit((char) c));
  }

  /**
   * Validates whether the entered integer is between 1 and 10 or is -1.
   *
   * @param integer The integer entered to be validated.
   * @return True if the integer is between 1 and 10 or is -1.
   *          False otherwise.
   */
  public boolean validateTrack(int integer) {
    return (integer >= 1 && integer <= 10) || integer == -1;
  }

  /**
   * Validates whether delay's hour exceeds the maxHours, or if the delay's hour is the same
   * as maxHours, but the delay's minutes exceeds the maxMinutes.
   *
   * @param departureTime The departure time that is taken into account.
   * @param delay The delay entered by the user
   * @return False if the delay's hour do not exceed the maxHours and if the delay's hour
   *          is the sameas maxHours, as well as the delay's minutes do not exceed the
   *          maxMinutes.
   *          True if otherwise.
   */
  public boolean validateAdjustedDepartureTime(LocalTime departureTime, LocalTime delay) {
    boolean validateAdjustedDepartureTime;
    int maxHours = 23 - departureTime.getHour();
    int maxMinutes = 59 - departureTime.getMinute();

    //Checks if the delay's hour exceeds the maxHours, or if the delay's hour is the same
    // as maxHours, but the delay's minutes exceeds the maxMinutes.
    validateAdjustedDepartureTime = delay.getHour() > maxHours
        || (delay.getHour() == maxHours && delay.getMinute() > maxMinutes);
    return validateAdjustedDepartureTime;
  }
}
